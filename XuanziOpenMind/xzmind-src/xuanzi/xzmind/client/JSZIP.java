package xuanzi.xzmind.client;

public class JSZIP {

	public JSZIP() {
		init(); 
	}
	
	public native void init()/*-{
		this.jszip = new $wnd.JSZip();
	}-*/;
	
	public native void file(String file,String text)/*-{
		this.jszip.file(file, text);
	}-*/;
	
	public native JSZIP remove(String folder)/*-{
		 this.jszip.remove(folder);
		 return this;
	}-*/;
	
	public native JSZIP folder(String folder)/*-{
		 this.jszip.folder(folder);
		 return this;
	}-*/;
	
	public native JSZIP download()/*-{
		 	this.jszip.generateAsync({type:"base64"}).then(function (base64) {
		        window.location = "data:application/zip;base64," + base64;
		    }, function (err) {
		        console.log(err);
		    });
		 return this;
	}-*/;

}
